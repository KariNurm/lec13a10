import express, {Request, Response}  from 'express'
import dao from '../dao'

interface Comment {
	userId: number
	postId: number
	content: string
	date: string
}

const commentsRouter = express.Router()

//GET comments by user id
commentsRouter.get('/:userId', async (req: Request, res: Response ) => {
	const userId = req.params.userId
	const results = await dao.commentsByUserId(userId)
	res.send(results.rows)
})

//POST a new comment
commentsRouter.post('/', async (req: Request, res: Response ) => {
	if(!req.body) return res.send('Missing body')
	
	const comment: Comment = req.body
	await dao.newComment(comment)
	res.status(201).send('Comment posted!')
})

//DELETE a comment
commentsRouter.delete('/:commentId', async (req: Request, res: Response ) => {
	const commentId = req.params.commentId
	await dao.deleteComment(commentId)
	res.send('Comment deleted!')
})

export default commentsRouter