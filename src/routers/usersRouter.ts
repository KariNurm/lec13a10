import express, {Request, Response}  from 'express'
import dao from '../dao'

interface User {
	username: string
	fullName: string
	email: string
}

const usersRouter = express.Router()

//GET all users
usersRouter.get('/', async (_req: Request, res: Response ) => {
	const results = await dao.allUsers()
	res.send(results.rows)
})


//GET user by id
usersRouter.get('/:id', async (req: Request, res: Response ) => {
	const id = req.params.id
	const results = await dao.userById(id)
	res.send(results.rows)
})


//POST a new user
usersRouter.post('/', async (req: Request, res: Response ) => {
	if(!req.body) return res.send('Missing body')
	
	const user: User = req.body
	await dao.addUser(user)
	res.status(201).send('New user added!')
})


//DELETE a user
usersRouter.delete('/:userId', async (req: Request, res: Response ) => {
	const userId = req.params.userId
	await dao.deleteUser(userId)
	res.send('User deleted!')
})


export default usersRouter