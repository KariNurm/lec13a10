import express, {Request, Response}  from 'express'

const versionRouter = express.Router()

//GET all users
versionRouter.get('/', async (_req: Request, res: Response ) => {
	res.send('version 1.0')
})


export default versionRouter