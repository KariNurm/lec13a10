import express, {Request, Response}  from 'express'
import dao from '../dao'

interface Post {
	userId: number
	title: string
	content: string
	date: string
}

const postsRouter = express.Router()

//GET all posts
postsRouter.get('/', async (_req: Request, res: Response ) => {
	const results = await dao.allPosts()
	res.send(results.rows)
	
})


//GET post by id
postsRouter.get('/:id', async (req: Request, res: Response ) => {
	const id = req.params.id
	const results = await dao.postById(id)
	res.send(results.rows)
})


//POST a new post
postsRouter.post('/', async (req: Request, res: Response ) => {
	if(!req.body) return res.send('Missing body')

	const post: Post = req.body
	await dao.addPost(post)
	res.status(201).send('New post added!')
})

//DELETE a post
postsRouter.delete('/:postId', async (req: Request, res: Response ) => {
	const postId = req.params.postId
	await dao.deletePost(postId)
	res.send('Post deleted!')
})


export default postsRouter