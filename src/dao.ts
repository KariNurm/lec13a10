import { executeQuery } from './db'
import query from './query'

interface User {
	username: string
	fullName: string
	email: string
}

interface Post {
	userId: number
	title: string
	content: string
	date: string
}

interface Comment {
	userId: number
	postId: number
	content: string
	date: string
}

//Users 
const allUsers = async () => {	
	return await executeQuery(query.allUsers)
}

const userById = async (id:string) => {
	const params = [id]
	return await executeQuery(query.userById, params)
}

const addUser = async (user: User) => {
	const params = [user.username, user.fullName, user.email]
	const results = await executeQuery(query.addUser, params)
	return results
}

const deleteUser = async (userId: string) => {
	const params = [userId]
	const results = await executeQuery(query.deleteUser, params)
	return results
}

//Posts
const allPosts = async () => {	
	return await executeQuery(query.allPost)
}

const postById = async (id:string) => {
	const params = [id]
	return await executeQuery(query.postById, params)
}

const addPost = async (post: Post) => {
	const currentDate = new Date().toISOString().substring(0, 10)
	const params = [post.userId, post.title, post.content, currentDate]
	return await executeQuery(query.addPost, params)
}

const deletePost = async (postId: string) => {
	const params = [postId]
	await executeQuery(query.deletePostComments, params)
	await executeQuery(query.deletePost, params)
	return 
}


//Comments
const commentsByUserId = async (userId: string) => {	
	const params = [userId]
	return await executeQuery(query.commentsByUserId, params)
}

const newComment = async (comment: Comment) => {
	const currentDate = new Date().toISOString().substring(0, 10)
	const params = [comment.userId, comment.postId, comment.content, currentDate]
	return await executeQuery(query.addComment, params)
}

const deleteComment = async (commentId: string) => {
	const params = [commentId]
	const results = await executeQuery(query.deleteComment, params)
	return results
}


export default { 
	allUsers,
	userById, 
	addUser, 
	deleteUser, 
	allPosts, 
	postById, 
	addPost, 
	deletePost, 
	commentsByUserId, 
	newComment, 
	deleteComment
}