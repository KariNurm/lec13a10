import express from 'express'
import usersRouter from './routers/usersRouter'
import postsRouter from './routers/postsRouter'
import commentsRouter from './routers/commentsRouter'
import versionRouter from './routers/versionRouter'
import { createForum } from './db'
import { notFound } from './middlewares'

const server = express()
server.use(express.json())

createForum()

server.use('/forum', express.static('public'))
server.use('/version', versionRouter)
server.use('/forum/users', usersRouter)
server.use('/forum/posts', postsRouter)
server.use('/forum/comments', commentsRouter)

server.use(notFound)


export default server