
// Users
const allUsers = 'SELECT id, username FROM users;' 
const userById = 'SELECT id, username, full_name, email FROM users WHERE users.id = $1'
const addUser = `
	INSERT INTO users (username, full_name, email)
	VALUES ($1, $2, $3);`
const deleteUser = 'DELETE FROM users WHERE users.id = $1'
	
// Posts
const allPost = 'SELECT id, user_id, title FROM posts'
const postById = `
	SELECT
		posts.id AS post_id,
    posts.title AS post_title,
    posts.content AS post_content,
    posts.post_date AS post_date,
    comments.id AS comment_id,
    comments.content AS comment_content,
    comments.comment_date AS comment_date
	FROM
		posts
	LEFT JOIN comments ON 
		posts.id = comments.post_id
	WHERE
		posts.id = $1;`
const addPost = `
	INSERT INTO posts (user_id, title, content, post_date)
	VALUES ($1, $2, $3, $4)`
const deletePostComments = 'DELETE FROM comments WHERE post_id = $1'
const deletePost = 'DELETE FROM posts WHERE posts.id = $1'

// Comments
const commentsByUserId = 'SELECT * FROM comments WHERE user_id = $1'
const addComment = `
	INSERT INTO comments (user_id, post_id, content, comment_date)
	VALUES ($1, $2, $3, $4)`
const deleteComment = 'DELETE FROM comments WHERE comments.id = $1'


// Initial
const initiate = `
CREATE TABLE IF NOT EXISTS users (
	id SERIAL PRIMARY KEY,
	username VARCHAR UNIQUE NOT NULL,
	full_name VARCHAR NOT NULL,
	email VARCHAR UNIQUE NOT NULL,
	password_hash VARCHAR
);
CREATE TABLE IF NOT EXISTS posts (
	id SERIAL PRIMARY KEY,
	user_id INT NOT NULL,
	title VARCHAR NOT NULL,
	content TEXT NOT NULL,
	post_date TIMESTAMP NOT NULL,
	FOREIGN KEY (user_id) REFERENCES users (id)
);
CREATE TABLE IF NOT EXISTS comments (
	id SERIAL PRIMARY KEY,
	user_id INT NOT NULL,
	post_id INT NOT NULL,
	content TEXT NOT NULL,
	comment_date TIMESTAMP NOT NULL,
	FOREIGN KEY (user_id) REFERENCES users (id),
	FOREIGN KEY (post_id) REFERENCES posts (id)
);`

export default {
	allUsers,
	userById,
	addUser,
	deleteUser,
	addPost, 
	allPost, 
	deletePost,
	deletePostComments,
	commentsByUserId, 
	postById,
	initiate,
	addComment,
	deleteComment
}