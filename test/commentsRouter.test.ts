import { pool } from '../src/db'
import { jest } from '@jest/globals'
import server from '../src/server'
import request from 'supertest'


const initializeMockPool = (mockResponse: any) => {
	(pool as any).connect = jest.fn(() => {
		return {
			query: () => mockResponse,
			release: () => null
		}
	})
}

describe('Testing GET /comments by userid', () => {
	const mockResponse = {
		rows: [
			{ id: 1, user_id: 101, post_id: 5, content: 'hi', comment_date: '2000-12-12' },
			{ id: 2, user_id: 101, post_id: 6, content: 'hi again', comment_date: '2000-12-11' }
		]
	}

	beforeAll(() => {
		initializeMockPool(mockResponse)
	})

	afterAll(() => {
		jest.clearAllMocks()
	})

	it('', async () => {
		const response = await request(server)
			.get('/forum/comments/101')
			
		expect(response.status).toBe(200)
		expect(response.body).toStrictEqual(mockResponse.rows)
	})
})

describe('Testing POST /comments', () => {

	it('returns 201', async () => {

		const response = await request(server)
		.post('/forum/comments')
    .send({'userId': 8, 'postId': 3, 'content': 'test'})
		expect(response.status).toBe(201)
	})
  
})
describe('Testing DELETE /comments', () => {
	
	
	it('returns 200', async () => {

		const response = await request(server)
		.delete('/forum/comments/1')
		expect(response.status).toBe(200)
	})
  
})