import { pool } from '../src/db'
import { jest } from '@jest/globals'
import server from '../src/server'
import request from 'supertest'


const initializeMockPool = (mockResponse: any) => {
	(pool as any).connect = jest.fn(() => {
		return {
			query: () => mockResponse,
			release: () => null
		}
	})
}

describe('Testing GET /posts', () => {
	const mockResponse = {
		rows: [
			{ id: 101, user_id: 1, title: 'Test Item 1'},
			{ id: 102, user_id: 2, title: 'Test Item 2' }
		]
	}

	beforeAll(() => {
		initializeMockPool(mockResponse)
	})

	afterAll(() => {
		jest.clearAllMocks()
	})

	it('returns all posts', async () => {
		const response = await request(server)
			.get('/forum/posts')
			
		expect(response.status).toBe(200)
		expect(response.body).toStrictEqual(mockResponse.rows)
	})
})

describe('Testing GET /posts by user', () => {
	const mockResponse = {
		rows: [
			{ id: 101, post_id: 1, post_title: 'Test Item 1', post_content: 'jee', post_date: '2000-12-12', comment_id: 1, comment_content: 'content', comment_date: '2000-12-12'},
			{ id: 102, post_id: 1, post_title: 'Test Item 2', post_content: 'jee2', post_date: '2001-12-12', comment_id: null, comment_content: null, comment_date: null}
		]
	}

	beforeAll(() => {
		initializeMockPool(mockResponse)
	})

	afterAll(() => {
		jest.clearAllMocks()
	})

	it('returns posts by user', async () => {
		const response = await request(server)
			.get('/forum/posts/1')
			
		expect(response.status).toBe(200)
		expect(response.body).toStrictEqual(mockResponse.rows)
	})
})


describe('Testing POST /posts', () => {
	
	it('return 201', async () => {
		const response = await request(server)
			.post('/forum/posts/')
			
		expect(response.status).toBe(201)
	})
})

describe('Testing DELETE /posts by id', () => {

	it('return 200', async () => {
		const response = await request(server)
			.delete('/forum/posts/1')
			
		expect(response.status).toBe(200)
	})
})