import { pool } from '../src/db'
import { jest } from '@jest/globals'
import server from '../src/server'
import request from 'supertest'


const initializeMockPool = (mockResponse: any) => {
	(pool as any).connect = jest.fn(() => {
		return {
			query: () => mockResponse,
			release: () => null
		}
	})
}

describe('Testing GET /users', () => {
	const mockResponse = {
		rows: [
			{ id: 101, username: 'Test Item 1' },
			{ id: 102, usernamename: 'tester2' }
		]
	}

	beforeAll(() => {
		initializeMockPool(mockResponse)
	})

	afterAll(() => {
		jest.clearAllMocks()
	})

	it('gets all users', async () => {
		const response = await request(server)
			.get('/forum/users')
		expect(response.status).toBe(200)
		expect(response.body).toStrictEqual(mockResponse.rows)
	})
})

describe('Testing GET /users by id', () => {
	const mockResponse = {
		rows: [
			{ id: 101, username: 'Test Item 1', full_name: 'tester1', email: 'hotmail@' }
		]
	}

	beforeAll(() => {
		initializeMockPool(mockResponse)
	})

	afterAll(() => {
		jest.clearAllMocks()
	})

	it('returns one user', async () => {
		const response = await request(server)
			.get('/forum/users/101')
		expect(response.status).toBe(200)
		expect(response.body).toStrictEqual(mockResponse.rows)
	})
})

describe('Testing POST /users', () => {
	
	it('returns 201', async () => {

		const response = await request(server)
		.post('/forum/users')
    .send({'username': 'Bill', 'fullName': 'Bill Pepper', 'email': 'email@'})
		expect(response.status).toBe(201)
	})
  
})

describe('Testing DELETE /users', () => {
	
	it('returns 200', async () => {

		const response = await request(server)
		.delete('/forum/users/1')
		expect(response.status).toBe(200)
	})
  
})